import { Injectable } from '@angular/core';
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';
@Injectable()
export class UserService {

    private isUserLoggedIn: boolean;
    public username: string;
    //public isLogedIn: boolean;

    constructor( private storage: LocalStorageService) {

        this.isUserLoggedIn = this.storage.retrieve('user');
    }

    //ngOnInit() {
    //    this.localSt.observe('key')
    //        .subscribe((value) => console.log('new value', value));
    //}


    //constructor() {
    //    //if (localStorage.getItem("user") === null) {
    //    //    this.isUserLoggedIn = false;

    //    //} else {
    //       this.isUserLoggedIn = false;
    //    //}
    //}

    setUserLoggedIn() {
        //debugger;
        this.isUserLoggedIn = true;
        this.username = 'admin';
        this.storage.store('user', this.isUserLoggedIn);
        // localStorage.setItem('id_token', this.isUserLoggedIn);
        //localStorage.setItem('user', JSON.stringify(this.isUserLoggedIn));

    }

    getUserLoggedIn() {
        //debugger;
        //if (localStorage.getItem('user')) {
        //    this.isUserLoggedIn = false;

        //} else {
        //    this.isUserLoggedIn = false;
        //}
        return this.isUserLoggedIn;
    }

}
