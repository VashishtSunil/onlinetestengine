import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { Ng2Webstorage } from 'ngx-webstorage';
import { BrowserModule } from '@angular/platform-browser'

import { AppComponent } from './components/app/app.component';
import { NavMenuComponent } from './components/navmenu/navmenu.component';
import { HomeComponent } from './components/home/home.component';
import { FetchDataComponent } from './components/fetchdata/fetchdata.component';
import { CounterComponent } from './components/counter/counter.component';
import {LoginComponent } from './components/login/login.component';
import { TestComponent } from './components/test/test.component';
import { AddQuestionComponent } from './components/addQuestion/addQuestion.component';

import { AuthguardGuard } from './authguard.guard';
import { UserService } from './user.service'


@NgModule({
    declarations: [

        AppComponent,
        NavMenuComponent,
        CounterComponent,
        FetchDataComponent,
        HomeComponent,
        TestComponent,
        LoginComponent,
        AddQuestionComponent
    ],
    imports: [
        BrowserModule,
        CommonModule,
        HttpModule,
        FormsModule,
        Ng2Webstorage,
        ReactiveFormsModule,
        RouterModule.forRoot([
            { path: '', redirectTo: 'login', pathMatch: 'full' },
            { path: 'home', component: HomeComponent },
            {
                path: 'test',
                canActivate: [AuthguardGuard],
                component: TestComponent
            },
            
            { path: 'addQuestion', component: AddQuestionComponent },
            { path: 'login', component: LoginComponent },
            { path: 'counter', component: CounterComponent },
            { path: 'fetch-data', component: FetchDataComponent },
            { path: '**', redirectTo: 'login' }
        ])
    ],
    providers: [UserService, AuthguardGuard]
})
export class AppModuleShared {
}
