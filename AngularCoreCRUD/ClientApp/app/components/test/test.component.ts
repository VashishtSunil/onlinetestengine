import { Component, Inject } from '@angular/core';
import { Http } from '@angular/http';
import { UserService } from '../../user.service';

@Component({
    selector: 'test',
    templateUrl: './test.component.html'
})

export class TestComponent {
    public questions: Questions[];
    public questionNo = 0;
    public totaQuestions = 0;
    public correctQuestions = 0;
    public showResult: boolean;
    name = 'anony';
    //
    constructor(http: Http, @Inject('BASE_URL') baseUrl: string, private user: UserService) {
        this.showResult = false;
        http.get(baseUrl + 'api/Questions').subscribe(result => {
            this.questions = result.json() as Questions[];
        }, error => console.error(error));
    }
    //
    ngOnInit() {
        this.name = this.user.username;
        console.log('Is user logged in? ', this.user.getUserLoggedIn())
    }
    //
    submitTest() {
        this.questions.forEach(
            childObj => {
                this.totaQuestions++;
                if (childObj.correctAns == "A" && childObj.ans1Check) {
                    this.correctQuestions++;
                }
                if (childObj.correctAns == "B" && childObj.ans2Check) {
                    this.correctQuestions++;
                }
                if (childObj.correctAns == "c" && childObj.ans3Check) {
                    this.correctQuestions++;
                }
                if (childObj.correctAns == "D" && childObj.ans4Check) {
                    this.correctQuestions++;
                }
                this.showResult = true;
            }
        );
    }
}

interface Questions {
    question: string;
    ans1: string;
    ans1Check: boolean;
    ans2Check: boolean;
    ans3Check: boolean;
    ans4Check: boolean;
    ans2: string;
    ans3: string;
    ans4: string;
    correctAns: string;
    ansSelected: string;

}