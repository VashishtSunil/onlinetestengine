import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../../user.service';
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';
@Component({
    selector: 'app',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {

    public isLogedIn: boolean;

    constructor(private user: UserService, private storage: LocalStorageService) { 
        this.isLogedIn = this.storage.retrieve('user');
    }
    //
    ngOnInit() {
        this.isLogedIn = this.user.getUserLoggedIn();
       // console.log('Is user logged in? ', )
    }



}
