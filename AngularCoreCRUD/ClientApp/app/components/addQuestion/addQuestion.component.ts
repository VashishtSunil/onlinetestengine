import { Component, Inject } from '@angular/core';
import { FormsModule, ReactiveFormsModule, FormGroup, FormControl, Validators } from '@angular/forms';
import { Http } from '@angular/http';
import { UserService } from '../../user.service';
@Component({
    selector: 'addQuestion',
    templateUrl: './addQuestion.component.html'
})
export class AddQuestionComponent {
    public frmAddQuestion: FormGroup;
    constructor(private http: Http, @Inject('BASE_URL') baseUrl: string, private user: UserService) {
       
     
    }
    ngOnInit() {
        this.frmAddQuestion = new FormGroup({
           // question: new FormGroup({
            question: new FormControl('', [
              // Validators.minLength(8),
                Validators.required
            ]),
            ans1: new FormControl('', [
                // Validators.minLength(8),
                Validators.required
            ]),
             ans2: new FormControl('', [
                // Validators.minLength(8),
                Validators.required
            ]),
              ans3: new FormControl('', [
                 // Validators.minLength(8),
                 Validators.required
             ]),
               ans4: new FormControl('', [
                  // Validators.minLength(8),
                  Validators.required
              ])
            //,
                //lastName: new FormControl('', Validators.required),
           // })
            //,
            //email: new FormControl('', [
            //    Validators.required,
            //    Validators.pattern("[^ @]*@[^ @]*")
            //]),
            //password: new FormControl('', [
            //    Validators.minLength(8),
            //    Validators.required
            //]),
            //language: new FormControl()
        });
    }

    public onFormSubmit(): void {

        debugger;
        //console.log();
        this.http.post('http://localhost:52521/api/Questions', this.frmAddQuestion.value).subscribe(result => {
            //this.questions = result.json() as Questions[];
        }, error => console.error(error));
    }
   
}

