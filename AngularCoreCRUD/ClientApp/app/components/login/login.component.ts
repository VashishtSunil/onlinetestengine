import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../../user.service';
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';
//imports above
declare var window: any;
declare var FB: any;

@Component({
    selector: 'login',
    templateUrl: './login.component.html'
})
export class LoginComponent {
    private isUserLoggedIn: boolean;
    //

    constructor(private router: Router, private user: UserService, private storage: LocalStorageService) {
        this.isUserLoggedIn = this.storage.retrieve('user');
        if (this.isUserLoggedIn) {
            this.router.navigate(['test']);
        }

    }

    //
    ngOnInit() {
        console.log('hit');
    }

    //
    loginUser(e: any) {
        e.preventDefault();
        console.log(e);
        var username = e.target.elements[0].value;
        var password = e.target.elements[1].value;
        if (username == 'admin' && password == 'admin') {
            debugger;
            this.user.setUserLoggedIn();
            this.router.navigate(['test']);
        }
    }
}
