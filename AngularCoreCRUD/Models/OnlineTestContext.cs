﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace AngularCoreCRUD.Models
{
    public partial class OnlineTestContext : DbContext
    {
        public virtual DbSet<Questions> Questions { get; set; }
        public OnlineTestContext(DbContextOptions<OnlineTestContext> options)
    : base(options)
        { }
        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    if (!optionsBuilder.IsConfigured)
        //    {
        //        //#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
        //        optionsBuilder.UseSqlServer(@"Server=DESKTOP-M7UJS1L;Database=OnlineTest;Trusted_Connection=True;");
        //    }
        //}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Questions>(entity =>
            {
                entity.Property(e => e.Ans1)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Ans2)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Ans3)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Ans4)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CorrectAns)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.ModifiedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Question).IsUnicode(false);
            });
        }
    }
}
